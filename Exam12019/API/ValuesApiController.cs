﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam12019.API
{
    [Route("api/Restaurant")]
    [ApiController]
    public class ValuesApiController : ControllerBase
    {
        private readonly RestaurantServiceRedis _restaurantMan;

        public ValuesApiController(RestaurantServiceRedis restaurantServiceRedis)
        {
            this._restaurantMan = restaurantServiceRedis;
        }

        // GET: api/ValuesApi
        [HttpGet(Name = "Get Restaurant")]
        public async Task<ActionResult<List<RestaurantViewModel>>> GetAllRestaurant()
        {
            var allData = await _restaurantMan.GetRestaurantAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        // GET: api/ValuesApi/5
        [HttpGet("{id}", Name = "Get Restaurant by ID")]
        public async Task<ActionResult<RestaurantViewModel>> GetAllRestaurantbyId(Guid id)
        {
            var allData = await _restaurantMan.GetRestaurantAsync();
            var findData = allData.Where(Q => Q.restaurantID == id).FirstOrDefault();
            if (findData == null)
            {
                return NotFound();
            }
            return findData;
        }

        // POST: api/ValuesApi
        [HttpPost(Name = "Insert Restaurant")]
        public async Task<bool> InsertRestaurant([FromBody] RestaurantViewModel parameterRestaurant)
        {
            if (ModelState.IsValid == false)
            {
                return false;
            }
            await _restaurantMan.CreateRestaurantAsync(parameterRestaurant);
            return true;
        }

        // PUT: api/ValuesApi/5
        [HttpPost("{id}", Name = "Update Restaurant")]
        public async Task<ActionResult> UpdateRestaurant(Guid id, [FromBody] RestaurantViewModel parameterRestaurant)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }
            var allData = await _restaurantMan.GetRestaurantAsync();
            var findData = allData.FindIndex(Q => Q.restaurantID == id);
            if (findData == -1)
            {
                return NotFound();
            }
            allData[findData] = parameterRestaurant;
            await _restaurantMan.UpdateRestaurantAsync(allData);
            return Ok();
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}", Name = "Delete Restaurant")]
        public async Task<ActionResult> DeleteRestaurant(Guid id)
        {
            var allData = await _restaurantMan.GetRestaurantAsync();
            var findData = allData.FindIndex(Q => Q.restaurantID == id);
            if (findData == -1)
            {
                return NotFound();
            }
            allData.RemoveAt(findData);
            await _restaurantMan.UpdateRestaurantAsync(allData);
            return Ok();

        }
    }
}
