﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/User")]
    [ApiController]
    public class UserApiController : ControllerBase
    {
        private readonly LoginService _loginMan;

        public UserApiController(LoginService loginService)
        {
            this._loginMan = loginService;
        }
        // GET: api/<controller>
        [HttpGet(Name ="Get User")]
        public async Task<ActionResult<List<UserViewModel>>> GetAllUser()
        {
            var allData = await _loginMan.GetUserAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        // GET api/<controller>/5
        [HttpGet("{id}",Name = "Get User by ID")]
        public async Task<ActionResult<UserViewModel>> GetAllUserbyId(Guid id)
        {
            var allData = await _loginMan.GetUserAsync();
            var findData = allData.Where(Q => Q.ID == id).FirstOrDefault();
            if (findData == null)
            {
                return NotFound();
            }
            return findData;
        }

        // POST api/<controller>
        [HttpPost(Name = "Insert User")]
        public async Task<bool> InsertUser([FromBody] UserViewModel parameterUser)
        {
            if (ModelState.IsValid == false)
            {
                return false;
            }
            await _loginMan.InsertUserAsync(parameterUser);
            return true;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
