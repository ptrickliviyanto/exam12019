﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/Reservation")]
    [ApiController]
    public class ReservationApiController : ControllerBase
    {
        private readonly ReservationServiceRedis _reservationMan;

        public ReservationApiController(ReservationServiceRedis reservationServiceRedis)
        {
            this._reservationMan = reservationServiceRedis;
        }

        // GET: api/<controller>
        [HttpGet(Name ="Get Reservation")]
        public async Task<ActionResult<List<ReservationViewModel>>> GetAllReservation()
        {
            var allData = await _reservationMan.GetReservationAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        // GET api/<controller>/5
        [HttpGet("{id}",Name = "Get Reservation by ID")]
        public async Task<ActionResult<ReservationViewModel>> GetAllRestaurantbyId(Guid id)
        {
            var allData = await _reservationMan.GetReservationAsync();
            var findData = allData.Where(Q => Q.reservationID == id).FirstOrDefault();
            if (findData == null)
            {
                return NotFound();
            }
            return findData;
        }

        // POST api/<controller>
        [HttpPost(Name ="Insert Reservation")]
        public async Task<bool> InsertReservation([FromBody] ReservationViewModel parameterReservation)
        {
            if (ModelState.IsValid == false)
            {
                return false;
            }
            await _reservationMan.CreateReservationAsync(parameterReservation);
            return true;
        }


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
