using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly LoginService _loginMan;

        //Untuk mengambil data dari form.
        [BindProperty]
        public UserViewModel UserData { set; get; }
        //Menampilkan error message.
        [TempData]
        public string ErrorMessage { set; get; }

        public LoginModel(LoginService loginService) {
            this._loginMan = loginService;
        }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var allData = await _loginMan.GetUserAsync();
            var findIDData = allData.Where(Q => Q.username == UserData.username).FirstOrDefault();
            if (findIDData == null)
            {
                return Page();
            }

            if(ModelState.IsValid == false)
            {
                return Page();
            }

            var valid = UserData.username == findIDData.username && UserData.password == findIDData.password;

            if(valid == false)
            {
                ModelState.AddModelError("UserData.password", "Invalid Login Email or Password");
                return Page();
            }
            UserData.role = findIDData.role;

            var id = new ClaimsIdentity("LoginClaim");
            id.AddClaim(new Claim(ClaimTypes.Name, UserData.username));
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, UserData.username));
            id.AddClaim(new Claim(ClaimTypes.Role, UserData.role));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("LoginClaim", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
                IsPersistent = true
            });
            if(UserData.role == "Administrator")
            {
                return RedirectToPage("../View");
            }
            return RedirectToPage("../Index");
        }
    }
}