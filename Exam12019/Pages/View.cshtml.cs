using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    //Page hanya bisa di buka oleh user yang memiliki role Administrator
    [Authorize(Roles = "Administrator")]
    public class ViewModel : PageModel
    {
        private readonly IHttpClientFactory _httpFac;
        private readonly RestaurantServiceRedis _restaurantMan;

        [BindProperty]
        public List<RestaurantViewModel> Items { set; get; }

        public ViewModel(IHttpClientFactory httpClientFactory, RestaurantServiceRedis restaurantServiceRedis)
        {
            this._httpFac = httpClientFactory;
            this._restaurantMan = restaurantServiceRedis;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/Restaurant";
            var client = _httpFac.CreateClient();
            var response = await client.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed!");
            }
            Items = await response.Content.ReadAsAsync<List<RestaurantViewModel>>();
            return Page();
        }
    }
}