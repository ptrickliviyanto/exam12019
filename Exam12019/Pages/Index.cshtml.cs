﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IHttpClientFactory _httpFac;
        private readonly RestaurantServiceRedis _restaurantMan;

        [BindProperty]
        public List<RestaurantViewModel> Items { set; get; }
        [BindProperty]
        public List<ReservationViewModel> ReserveItem { set; get; }
        [BindProperty]
        public Guid searchID { get; set; }
        [BindProperty]
        public ReservationViewModel data { get; set; }

        /// <summary>
        /// Dependency injection.
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="restaurantServiceRedis"></param>
        public IndexModel(IHttpClientFactory httpClientFactory, RestaurantServiceRedis restaurantServiceRedis)
        {
            this._httpFac = httpClientFactory;
            this._restaurantMan = restaurantServiceRedis;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            //Untuk menampilkan data restaurant dan data reservation
            var apiUrl = "http://localhost:50508/api/Restaurant";
            var apiUrlReserve = "http://localhost:50508/api/Reservation";
            var client = _httpFac.CreateClient();
            var response = await client.GetAsync(apiUrl);
            var response2 = await client.GetAsync(apiUrlReserve);

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed!");
            }
            if(response2.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data reserve via API Failed!");
            }
            Items = await response.Content.ReadAsAsync<List<RestaurantViewModel>>();
            ReserveItem = await response2.Content.ReadAsAsync<List<ReservationViewModel>>();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/Reservation";
            var client = _httpFac.CreateClient();
            var apiUrl2 = "http://localhost:50508/api/Restaurant";
            var response2 = await client.GetAsync(apiUrl2);

            var allData = await response2.Content.ReadAsAsync<List<RestaurantViewModel>>();

            var findData = allData.Where(Q => Q.restaurantID == searchID).FirstOrDefault();
            if(findData == null)
            {
                return BadRequest("Invalid Restaurant ID");
            }

            var response = await client.PostAsJsonAsync(apiUrl, new ReservationViewModel
            {
                reservationID = Guid.NewGuid(),
                reservationUserName = data.reservationUserName,
                email = data.email,
                reservationRestaurantName = findData.restaurantName,
                reservationRestaurantAddress = findData.restaurantAddress,
                reservationTime = DateTime.Now,
            });
            return RedirectToPage("./Index");

        }
    }
}
