using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    //Page hanya bisa di buka oleh user yang memiliki role Administrator
    [Authorize(Roles = "Administrator")]
    public class DeleteModel : PageModel
    {
        private readonly RestaurantServiceRedis _restaurantMan;
        private readonly IHttpClientFactory _httpFac;

        [BindProperty]
        public RestaurantViewModel item { set; get; }
        [BindProperty(SupportsGet = true)]
        public Guid id { set; get; }
        [BindProperty]
        public string itemName { set; get; }

        public DeleteModel(RestaurantServiceRedis restaurantServiceRedis, IHttpClientFactory httpClientFactory)
        {
            this._restaurantMan = restaurantServiceRedis;
            this._httpFac = httpClientFactory;
        }

        public async Task<ActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/Restaurant/" + id;
            var client = _httpFac.CreateClient();
            var response = await client.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }

            var content = await response.Content.ReadAsAsync<RestaurantViewModel>();
            itemName = content.restaurantName;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(RestaurantViewModel parameterRestaurant)
        {
            var apiUrl = "http://localhost:50508/api/Restaurant/" + id;
            var client = _httpFac.CreateClient();
            var response = await client.DeleteAsync(apiUrl);
            return RedirectToPage("./View");
        }
    }
}