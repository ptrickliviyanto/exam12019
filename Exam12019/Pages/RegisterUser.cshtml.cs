using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Service;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    public class RegisterUserModel : PageModel
    {
        private readonly LoginService _loginMan;
        private readonly IHttpClientFactory _httpFac;

        //untuk mendapatkan isi form
        [BindProperty]
        public UserViewModel userData { set; get; }

        public RegisterUserModel(LoginService loginService, IHttpClientFactory httpClientFactory)
        {
            this._loginMan = loginService;
            this._httpFac = httpClientFactory;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(UserViewModel parameterUser)
        {
            var apiUrl = "http://localhost:50508/api/User";
            var client = _httpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new UserViewModel
            {
                ID = Guid.NewGuid(),
                username = userData.username,
                password = userData.password,
                role = "Human",
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed!");
            }
            return RedirectToPage("./View");
        }
    }
}