﻿using Exam12019.ViewModel;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Service
{
    public class RestaurantServiceRedis
    {
        private readonly IDistributedCache _Cache;

        public RestaurantServiceRedis(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }

        /// <summary>
        /// Mengambil semua data restaurant.
        /// </summary>
        /// <returns></returns>
        public async Task<List<RestaurantViewModel>> GetRestaurantAsync()
        {
            var restaurantJson = await _Cache.GetStringAsync("RestaurantRedis");
            if (restaurantJson == null)
            {
                return new List<RestaurantViewModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantViewModel>>(restaurantJson);
        }
        /// <summary>
        /// Membuat restaurant baru.
        /// </summary>
        /// <param name="parameterRestaurant"></param>
        /// <returns></returns>
        public async Task CreateRestaurantAsync(RestaurantViewModel parameterRestaurant)
        {
            var restaurantRedis = await GetRestaurantAsync();
            if (restaurantRedis == null)
            {
                restaurantRedis = new List<RestaurantViewModel>();
            }
            restaurantRedis.Add(parameterRestaurant);
            var restaurantJson = JsonConvert.SerializeObject(restaurantRedis);
            await _Cache.SetStringAsync("RestaurantRedis", restaurantJson);
        }
        /// <summary>
        /// Melakukan update restaurant.
        /// </summary>
        /// <param name="parameterRestaurant"></param>
        /// <returns></returns>
        public async Task UpdateRestaurantAsync(List<RestaurantViewModel> parameterRestaurant)
        {
            var json = JsonConvert.SerializeObject(parameterRestaurant);
            await _Cache.SetStringAsync("RestaurantRedis", json);
        }
    }
}
