﻿using Exam12019.ViewModel;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Service
{
    public class LoginService
    {
        private readonly IDistributedCache _Cache;

        public LoginService(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }
        /// <summary>
        /// Mengambil semua data user.
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserViewModel>> GetUserAsync()
        {
            var valueJson = await _Cache.GetStringAsync("Users");
            if (valueJson == null)
            {
                return new List<UserViewModel>();
            }
            return JsonConvert.DeserializeObject<List<UserViewModel>>(valueJson);
        }
        /// <summary>
        /// Membuat user baru.
        /// </summary>
        /// <param name="parameterUser"></param>
        /// <returns></returns>
        public async Task InsertUserAsync(UserViewModel parameterUser)
        {
            var getAllData = await GetUserAsync();
            if (getAllData == null)
            {
                getAllData = new List<UserViewModel>();
            }
            getAllData.Add(parameterUser);
            var userJson = JsonConvert.SerializeObject(getAllData);
            await _Cache.SetStringAsync("Users", userJson);
        }
    }
}
