﻿using Exam12019.ViewModel;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Service
{
    public class ReservationServiceRedis
    {
        private readonly IDistributedCache _Cache;

        public ReservationServiceRedis(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }
        /// <summary>
        /// Mengambil semua data reservation.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReservationViewModel>> GetReservationAsync()
        {
            var reservationJson = await _Cache.GetStringAsync("ReservationRedis");
            if(reservationJson == null)
            {
                return new List<ReservationViewModel>();
            }
            return JsonConvert.DeserializeObject<List<ReservationViewModel>>(reservationJson);

        }

        public async Task CreateReservationAsync(ReservationViewModel parameterReservation)
        {
            var reservationRedis = await GetReservationAsync();
            if(reservationRedis == null)
            {
                reservationRedis = new List<ReservationViewModel>();
            }
            reservationRedis.Add(parameterReservation);
            var reservationJson = JsonConvert.SerializeObject(reservationRedis);
            await _Cache.SetStringAsync("ReservationRedis", reservationJson);
        }
    }
}
