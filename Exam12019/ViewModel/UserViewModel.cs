﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.ViewModel
{
    public class UserViewModel
    {
        [Key]
        public Guid ID { set; get; }
        [Required]
        public string username { set; get; }
        [Required]
        public string password { set; get; }
        public string role { set; get; }
    }
}
