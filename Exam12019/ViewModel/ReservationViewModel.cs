﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.ViewModel
{
    public class ReservationViewModel
    {
        [Key]
        [Required]
        public Guid reservationID { get; set; }
        [Required]
        [StringLength(255)]
        public string reservationUserName { get; set; }
        [Required]
        [StringLength(255)]
        public string email { get; set; }
        [Required]
        public string reservationRestaurantName { get; set; }
        [Required]
        public string reservationRestaurantAddress { get; set; }
        [Required]
        public DateTime reservationTime { get; set; }
    }
}
