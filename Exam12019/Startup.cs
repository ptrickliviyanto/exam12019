using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Swashbuckle.AspNetCore.Swagger;

namespace Exam12019
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Belajar API", Version = "v1" });
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<RestaurantServiceRedis>();
            services.AddScoped<ReservationServiceRedis>();
            services.AddScoped<LoginService>();
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "Exam12019";
            });

            var cm = ConnectionMultiplexer.Connect("localhost");
            services.AddDataProtection().PersistKeysToStackExchangeRedis(cm, "BelajarAspLoginEncryptionKey");
            services.AddHttpClient();
            services.AddHttpContextAccessor();

            services
                .AddAuthentication("LoginClaim")
                .AddCookie("LoginClaim", option=> {
                    option.LoginPath = "/Auth/Login";
                    option.LogoutPath = "/Auth/Logout";
                    option.AccessDeniedPath = "/Auth/Denied";
                });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Belajar API");
                c.DisplayOperationId();
            });
            app.UseMvc();
        }
    }
}
